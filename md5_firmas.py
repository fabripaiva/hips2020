#!/usr/bin/python
# -*- coding: utf-8 -*-
# coding=<salt>
from ImportsyConexiones import *

def firmas_md5 (lista_de_camineros):
        os.system("rm -rf 8.txt") 
	#en esta lista almacenaremos los archivos a analizar con firma MD5 para su verificacion y comparacion
        lista_temporal =[]
        for camino in lista_de_camineros: #recorremos los archivos que esta dentro de LISTA_DE_CAMINEROS
                if os.path.isdir(camino):  #en este punto verificaremos si la camino es archivo o carpeta, entonces si es carpeta se analizara con el vector la direccion completa de estos archivos
                        lista_temporal_temp = os.listdir(camino)
                        for elemento_lista in lista_temporal_temp: #elemento lista seria otra variable temporal donde se guardara el archivo para su agregado de elemento a nuestra otra lista
                                lista_temporal.append(camino + '/' + elemento_lista )#entonces aca agreamos el camino completo de esa carpeta a nuestra lista de directorios temporal
                else:   #si no, simplemente se prepara la lista como una de un elemento, la direccion abusoluta del unico archivo
                        lista_temporal = [camino]
                # Luego en este punto recorremos la lista temporal en busca de la suma md5 correspondiente de cada archivo o carpeta
                print(lista_temporal)
                for direccion in lista_temporal:      #para luego cargarlo en la base de datos
                        archivo_temp = hashlib.md5((open(direccion)).read()) #entonces con hashlib sacamos esta suma y lo guardamos en una variable
                        MD5suma = str(archivo_temp.hexdigest()) #convierte el valor en cadena
                        cursor.execute("SELECT ArchivoDireccion FROM archivos_Con_Firma_md5 WHERE ArchivoDireccion=%s", (direccion,)) #luego con curssor execute cargamos a la base de datos
                        existe_ban = cursor.fetchone() #Con fetchone recupera la siguiente fila de un conjunto de resultados de la consulta y devuelve una secuencia única o ninguna si no hay más 
							#filas disponibles. Por defecto, la tupla devuelta consiste en datos devueltos, convertidos en objetos Python. Si el cursor es un cursor sin formato, no se produce dicha conversión
                        if isinstance(existe_ban, tuple):   #Si existe el archivo buscado en la base de datos.
				#Mas bien la funcion devuelve TRUE si el objeto especificado es del tipo especificado, de lo contrario devolvera FALSE si no existe los elementos, se devolvera TRUE si el objeto es uno de los tipos de la trupla.
                                cursor.execute("SELECT md5sum FROM archivos_Con_Firma_md5 WHERE ArchivoDireccion=%s",(direccion,))
                                md5_BD= cursor.fetchone()[0] #asignamos el md5 del archivo de la base de datos a md5_BD
                                if md5_BD != MD5suma:    #Verificaremos si el archivo o carpeta consultado y el archivo de la base de datos tenga la misma firma MD5          
                                        fecha = time.strftime("%d/%m/%Y")  #guardamos la fecha y hora con strftime para su posterior uso
                                        hora = time.strftime("%H:%M:%S")
                                        mensaje = 'Alarma!! ---->  Se ha modificado un archivo o carpeta: ' #mensaje para el cuerpo que sera enviado al registro de logs de alarmas y al correo del admin
                                        entrada_mensaj = fecha + ' ====== ' + hora + '\n' + mensaje + ' ----> ' + direccion + '\n\n' #cuerpo completo del mensaje
                                        archivo=open('/var/log/hids/alarmashids.log', 'a') #abrimos el .log de alarmas para escribir la alarma
                                        archivo.write(str((entrada_mensaj)))
                                        #MANDAMOS CORREO AL ADMIN PARA ALERTAR DE POSIBLE AMENAZA CON EL USO DE SMTP
                                        #se procedera a desencriptar la contraseña de nuestro correo para luego realizar el envio de alerta con smtplib
                                        #En la carpeta tendremos un archivo encriptado con openssl, esto para que no haya ninguna contraseña visible
                                        os.system("openssl enc -aes-256-cbc -d -in pass_correo.txt.encrypted -out pass_correo.txt -k ADMINISTRATOR")
                                        pass_file = open("pass_correo.txt") #guardamos la contraseña en pass_file para luego esta ser reemplazada en otra variable y esta pasar de parametro en el smtplib
                                        input_pass_file = pass_file.read().replace('\n','') #se copia a esta variable para poner de parametro en la configuracion
                                        pass_file.close()# se cierra archivo
                                        os.system("rm -rf pass_correo.txt") #siempre eliminamos la contraseña del directorio para que no haya ninguna contraseña a la vista
                                        msg['Subject'] = "SE HA DETECTADO UNA ALARMA EN EL HIPS" #cabecera del mensaje
                                        msg.attach(MIMEText(entrada_mensaj, 'plain'))#cuerpo del mensaje
			               #configuramos los parametros para el envio con smtp
                                        server = smtplib.SMTP('smtp.gmail.com: 587')
                                        server.starttls()
                                        server.login(msg['From'], input_pass_file)
                                        server.sendmail(msg['From'], msg['To'], msg.as_string())#se realiza la ejecucion de envio
                                        server.quit()
                                        mensaje_md5 = 'echo "\nSe detecto una marca MD5 diferente, porfavor revisar el correo.\n" ' #avisamos en la terminal 
                                        mensaje2_md5= '\nSe detecto una marca MD5 diferente, porfavor revisar el correo\n'
                                        os.system(mensaje_md5)
                                        web = open("/home/fabrizio/Escritorio/hidss/HIPS2020/hips2020/hips2020/1.txt", "a")
                                        web.write(str((mensaje2_md5)))
                                        web.close()
                                        archivo.close()
                                else:
                                        print("Las firmas md5 son iguales")        
                        else:   #Entonces, si no existe el archivo o carpeta en la base de datos se realizara la alarma al correo y a alarma.logs
				    #Se vuelve a repetir la misma sintaxis que en cuerpo anterior, entonces ya no se explica.
                                mensaje = 'Alarma!!!! ---> El archivo que se cargo no esta en la base de datos, porfavor revisar'
                                fecha = time.strftime("%d/%m/%Y")
                                hora = time.strftime("%H:%M:%S")
                                entrada_mensaj = fecha + ' ====== ' + hora + '\n' + mensaje + ' ----> ' + direccion + '\n\n'
                                archivo=open('/var/log/hids/alarmashids.log', 'r+')
                                archivo.write(entrada_mensaj+'\n')
                                os.system("openssl enc -aes-256-cbc -d -in pass_correo.txt.encrypted -out pass_correo.txt -k ADMINISTRATOR")
                                pass_file = open("pass_correo.txt")
                                input_pass_file = pass_file.read().replace('\n','')
                                pass_file.close()
                                os.system("rm -rf pass_correo.txt")
                                msg['Subject'] = "SE HA DETECTADO UNA ALARMA EN EL HIPS"
                                msg.attach(MIMEText(entrada_mensaj, 'plain'))
                                server = smtplib.SMTP('smtp.gmail.com: 587')
                                server.starttls()
                                server.login(msg['From'], input_pass_file)
                                server.sendmail(msg['From'], msg['To'], msg.as_string())
                                server.quit()
                                mensaje_md5 = 'echo "\nSe detecto que un archivo cargado no esta registrado en la base de datos, porfavor verificar el correo\n"'
                                mensaje2_md5= 'Se detecto que un archivo cargado no esta registrado en la base de datos, porfavor verificar el correo'
                                os.system(mensaje_md5)
                                web = open("/home/fabrizio/Escritorio/hidss/HIPS2020/hips2020/hips2020/8.txt", "a")
                                web.write(str((mensaje2_md5)))
                                web.close()
                                archivo.close()
#firmas_md5(lista_de_camineros)

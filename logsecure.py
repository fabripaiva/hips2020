#!/usr/bin/python
# -*- coding: utf-8 -*-
# coding=<salt>
from ImportsyConexiones import *

def log_secure():
        os.system("rm -rf 7.txt")
        # Se extrae del archivo secure las entradas con 'Contraseña incorrecta'.
        contador_ip = [] #lo que haremos aca es guardar todas las entradas de ip asociada con un failed password, pertenencen a intentos fallidos por ingreso SSH
        archivo = os.popen('cat /home/fabrizio/Descargas/Ataque_SMTP_-_Secure.txt | grep "Contraseña Incorrecta"').read()#abrimos el archivo donde estan los ataques
        archivo = archivo.split(os.linesep) #En este punto separamos las lineas cuando se detecta un salto
        for linea_log in archivo[:-1]: #recorremos el archivo 
                ip_verif = linea_log.split(' ')[10]   # se procede a extraer la ip que no pudo acceder
		#print(ip_verif)
                ip_verif = linea_log.split(' ')[10]  # extraemos igual la ip que no pudo acceder
                fecha_verif = linea_log.split(' ')[0] + linea_log.split(' ')[1] #guardamos las fechas que aparecen en la cadenas 0 y 1
                contador_ip.append((fecha_verif,ip_verif)) #agregamos los elementos a la lista contador_ip 
        #Creamos un duo de variables {ip, cantidad de intentos de acceso}
        logs_ipcontador = [[x,contador_ip.count(x)] for x in set(contador_ip)]
        #print(logs_ipcontador)
        #asociamos el contador a verif y ahi hacemos la comparacion con las veces permitidas que son 3 veces
        for verif in logs_ipcontador:
                # Entonces si supera entra en el if para generar la alarma correspondiente o prevencion
                if verif[1] > N:         
                        mensaje = 'Se procedio a bloquear una IP por superar el limite de intentos via ssh2' #mensaje para el correo y el registro en logs/hids
                        fecha_verif = time.strftime("%d/%m/%Y") #guardamos fecha_verif y hora con strftime para usar en los mensajes del correo y en el log
                        hora = time.strftime("%H:%M:%S")
                        logs_mensaje = fecha_verif + ' ====== ' + hora + '\n' + mensaje + ' ----> ' + 'IP:' + verif[0][1] + '\n\n' #Mensaje completo que sera enviado al correo y al /var/hids
                        archivo = open('/var/log/hids/prevencionhids.log', 'a') #abrimos el archivo para escribir /var/log/hids
                        archivo.write(str((logs_mensaje))) #escribimos en el archivo
                        #se procedera a desencriptar la contraseña de nuestro correo para luego realizar el envio de alerta con smtplib
                        os.system("openssl enc -aes-256-cbc -d -in pass_correo.txt.encrypted -out pass_correo.txt -k ADMINISTRATOR")
                        pass_file = open("pass_correo.txt") #guardamos la contraseña en pass_file para luego esta ser reemplazada en otra variable y esta pasar de parametro en el smtplib
                        input_pass_file = pass_file.read().replace('\n','')
                        pass_file.close()
                        os.system("rm -rf pass_correo.txt")#siempre eliminamos la contraseña del directorio para que no haya ninguna contraseña a la vista
                        msg['Subject'] = "SE HA DETECTADO UNA ALARMA EN EL HIPS" #cabecera del correo
                        msg.attach(MIMEText(logs_mensaje, 'plain')) #cuerpo del Mensaje_Correo
                        server = smtplib.SMTP('smtp.gmail.com: 587') #configuramos los parametros para el envio con smtp
                        server.starttls()
                        server.login(msg['From'], input_pass_file)
                        server.sendmail(msg['From'], msg['To'], msg.as_string()) #se realiza la ejecucion de envio
                        server.quit()
                        mensaje_login = 'echo "\nSe detecto que una IP supero los limites de acceso via SSH2, porfavor revisar el correo\n" ' #Se avisa por terminal que ocurrio algo!
                        mensaje_alarma='Se detecto que una IP supero los limites de acceso via SSH2, porfavor revisar el correo' #mensaje para la alarma del registro del log
                        logs_alarma = fecha_verif + ' ====== ' + hora + '\n' + mensaje_alarma + '\n\n' #cuerpo del mensaje de alarma
                        archivo1 = open('/var/log/hids/alarmashids.log', 'a') #abrimos el archivo txt para escribir el registro
                        archivo1.write(str((logs_alarma)))
                        archivo1.close()
                        os.system(mensaje_login)
                        ip_bloqueadas.append(verif[0][1])
                        os.system('iptables -A INPUT -s ' + verif[0][1] + ' -j DROP')#Se bloquea el trafico de la ip con iptables 
                        archivo.close()
                        web = open("/home/fabrizio/Escritorio/hidss/HIPS2020/hips2020/hips2020/7.txt", "a")
                        web.write(str((logs_alarma)))
                        web.close()

        #ahora verificaremos los authentication failure
        contador_usuarios = [] #guardaremos las veces que el usuario intento entrar 
        archivo_ = os.popen('cat /home/fabrizio/Descargas/Ataque_SMTP_-_Secure.txt | grep "authentication failure;"').read() #leemos el archivo hacemos un filtrado en las lineas que aparecen authentication failure
        archivo_ = archivo_.split(os.linesep) #En este punto separamos las lineas cuando se detecta un salto
        for linea_log in archivo_[:-1]: #recorremos el archivo
                        usr_tmp = linea_log.split(' ')[14]    #identificamos el usuario que tuvo AF
                        fecha_verif = linea_log.split(' ')[0] + linea_log.split(' ')[1] #guardamos la fecha para verificar la cantidad de veces
                        contador_usuarios.append((fecha_verif,usr_tmp)) #agregamos los elementos a la lista contador_usuarios
        #Creamos un dupla de variables {usuario, cantidad de intentos de acceso}
        logs_UsuariosContador = [[x,contador_usuarios.count(x)] for x in set(contador_usuarios)]
        for verif in logs_UsuariosContador:
                #asociamos el contador a verif y ahi hacemos la comparacion con las veces permitidas que son 3 veces
                if verif[1] > N:  # Entonces si supera entra en el if para generar la alarma correspondiente o prevencion       
                        #si es un usuario entonces se procede a cambiar la contraseña con chpasswd
                        if not 'root' in verif[0][1][:-1]: #verif[0][1][:-1] es el nombre del usuario ubicado en la cadena 14
                                fecha_verif = time.strftime("%d/%m/%Y") #guardamos la fecha y hora con strftime
                                hora = time.strftime("%H:%M:%S")
                                nuevo_passw = ''.join([random.choice(string.ascii_letters + string.digits) for n in xrange(8)])  #funcion para obtener una contraseña con numeros y letras random con random.choice
                                os.system('echo "' + verif[0][1][:-1] +':'+ nuevo_passw + '" | chpasswd') #mediante os.system mandamos instrucciones de que cambie la contraseña con la contraseña que generamos arriba con chpasswd
                                mensaje = 'Se ha procedido a cambiar la contrasena de un usuario por superar los intentos al sistema root' #mensaje principal para el correo
                                logs_entrada = fecha_verif + ' ====== ' + hora + '\n' + mensaje + ' ----> ' + 'Usuario:' + verif[0][1][:-1] + ' ---> ' + nuevo_passw + '\n\n' #mensaje completo para el correo y para el /log/hids
                                consulta = "UPDATE users SET pass= %s WHERE usr= %s;" #con estos comandos reescribimos la contraseña del usuario en la base de datos de pg
                                cursor.execute(consulta,(nuevo_passw,verif[0][1][:-1]))#con estos comandos reescribimos la contraseña del usuario en la base de datos de pg
                                archivo = open('/var/log/hids/prevencionhids.log', 'a') #procedemos a escribir el registro en /log/hids
                                archivo.write(str((logs_entrada)))
                                #Esta parte es lo mismo que el cuerpo de mas arriba, en donde decripta la contraseña y realiza el envio al correo 
                                os.system("openssl enc -aes-256-cbc -d -in pass_correo.txt.encrypted -out pass_correo.txt -k ADMINISTRATOR")
                                pass_file = open("pass_correo.txt") #guardamos la contraseña en pass_file para luego esta ser reemplazada en otra variable y esta pasar de parametro en el smtplib
                                input_pass_file = pass_file.read().replace('\n','')
                                pass_file.close()
                                os.system("rm -rf pass_correo.txt")
                                msg['Subject'] = "SE HA DETECTADO UNA ALARMA EN EL HIPS"
                                msg.attach(MIMEText(logs_entrada, 'plain'))
                                server = smtplib.SMTP('smtp.gmail.com: 587')
                                server.starttls()
                                server.login(msg['From'], input_pass_file)
                                server.sendmail(msg['From'], msg['To'], msg.as_string())
                                server.quit()
                                mensaje_login = 'echo "\nSe detecto que un usuario supero los limites de acceso a root, porfavor revisar le correo\n" ' 
                                mensaje_alarma1='Se detecto que un usuario supero los limites de acceso a root, porfavor revisar le correo'
				#lo mismo que arriba, armamos el cuerpo para escribir la alarma en el log
                                logs_alarma1 = fecha_verif + ' ====== ' + hora + '\n' + mensaje_alarma1 + '\n\n'
                                archivo2 = open('/var/log/hids/alarmashids.log', 'a')
                                archivo2.write(str((logs_alarma1)))
                                archivo2.close()
                                os.system(mensaje_login)
                                pgDB_conexion.commit() #realiza el commit, o sea la actualizacion de la BD
                                archivo.close()
                                web = open("/home/fabrizio/Escritorio/hidss/HIPS2020/hips2020/hips2020/7.txt", "a")
                                web.write(str((logs_alarma1)))
                                web.close()

                        else:
                        #Entonces si fue el root notificamos igual al correo, esta vez sera una alarma de que hubo una violacion de acceso siendo el root (yo o alguuien mas) 
                                fecha_verif = time.strftime("%d/%m/%Y")
                                hora = time.strftime("%H:%M:%S")
                                mensaje = 'El usuario root supero los limites de acceso'
                                logs_entrada = fecha_verif + ' ====== ' + hora + '\n' + mensaje  + '\n\n'
                                archivo = open('/var/log/hids/alarmashids.log', 'a')
                                archivo.write(str((logs_entrada)))
                                #Esta parte es lo mismo que el cuerpo de mas arriba, en donde decripta la contraseña y realiza el envio al correo 
                                os.system("openssl enc -aes-256-cbc -d -in pass_correo.txt.encrypted -out pass_correo.txt -k ADMINISTRATOR")
                                pass_file = open("pass_correo.txt") #guardamos la contraseña en pass_file para luego esta ser reemplazada en otra variable y esta pasar de parametro en el smtplib
                                input_pass_file = pass_file.read().replace('\n','')
                                pass_file.close()
                                os.system("rm -rf pass_correo.txt")
                                msg['Subject'] = "SE HA DETECTADO UNA ALARMA EN EL HIPS"
                                msg.attach(MIMEText(logs_entrada, 'plain'))
                                server = smtplib.SMTP('smtp.gmail.com: 587')
                                server.starttls()
                                server.login(msg['From'], input_pass_file)
                                server.sendmail(msg['From'], msg['To'], msg.as_string())
                                server.quit()
                                mensaje_login = 'echo "\nSe detecto que el root supero el limite de intentos de acceso! Porfavor revisar el correo.\n" ' #aviso por terminal
                                os.system(mensaje_login)
                                archivo.close()
                                web = open("/home/fabrizio/Escritorio/hidss/HIPS2020/hips2020/hips2020/7.txt", "a")
                                web.write(str((logs_entrada)))
                                web.close()








#!/usr/bin/python
# -*- coding: utf-8 -*-
# coding=<salt>
from ImportsyConexiones import *

def ultimos_usuarios():
        os.system("rm -rf 2.txt") 
        #En un archivo quedata registrado los usuarios que ingresaron en el sistema en las ultimas horas odias
        #os.system("last > ultimos_usuarios.txt") #Podemos ejecutar un last o usar un archivo estatico de last en donde pondremos algunos usuarios que no estan registrados en la BD
        #Se modifica el archivo con la lista, manteniendo solo la informacion necesaria: Usuario y Direccion
        file = open('ultimos_usuarios.txt','r') #abrimos el archivo para su posterior recorrido
        #Recorremos el archivo, este recorrido es un recorrido especialmente al contenido del comando last, en python hay varios recorridos a este archivo en especifico, el de acontinuacion es uno de ellos.
	#En este recorrido, sacamos el nombre del usuario y su ip que estan en la cadena 0 y 2, (contando todos los espacios que estan respectivamente)        
	#entonces en este recorridos sacaremos el nombre del usuario y su ip para luego ponerlo en otro archivo que abriremos mas abajo	
	usuarios_ips_encontrados = '' #en esta lista se guardaran los usuarios y su ip
        for line in file:
                if(line == "\n"):
                        break
                i = 0
                while 1:
                        if(line[i] == ' '):
                                break
                        i+=1
                usr = line[:i]
                if(usr != "reboot"):
                        i+=1
                        tmp0 = line[i:]
                        i = 0
                        while 1:
                                if(tmp0[i] != ' '):
                                        break
                                i+=1
                        tmp1 = tmp0[i:]
                        i = 0
                        while 1:
                                if(tmp1[i] == ' '):
                                        break
                                i+=1
                        term = tmp1[:i]
                        tmp2 = tmp1[i:]
                        if("pts" in term):
                                i=0
                                while 1:
                                        if(tmp2[i] != ' '):
                                                break
                                        i+=1
                                tmp3 = tmp2[i:]
                                i = 0
                                while 1:
                                        if(tmp3[i] == ' '):
                                                break
                                        i+=1
                                addr = tmp3[:i]
                                newline = usr + '\t' + addr + '\n'
                                usuarios_ips_encontrados = usuarios_ips_encontrados + newline
                                print(usuarios_ips_encontrados)
        file.close() #cerramos el archivo
        #se guarda la lista modificada
        os.system("touch ultimos_usuarios1.txt") #ahora creamos el nuevo archivo con touch en donde alojaremos lo que esta adentro de usuarios_ip_encontrados 
        archivo_final = open('ultimos_usuarios1.txt', 'w')#abrimos archivo
        archivo_final.write(usuarios_ips_encontrados) #escribimos lo que esta adentro de usuarios_ip_encontrados
        archivo_final.close() #cerramos archivo
	#ordenamos un poco
        os.system("cat ultimos_usuarios1.txt | sort | uniq > ultimos_usuarios_final.txt") #mudamos lo que esta en ultimos_usuarios1 en un archivo final para poder eliminar los otros dos archivos y trabajar con solo uno 
        #os.system("rm -rf ultimos_usuarios.txt ultimos_usuarios1.txt")  #Para limpiar la carpeta de basura procedemos a borrar los archivos que creamos.
        #Cantidad de filas de la tabla users
        cursor.execute("SELECT COUNT(*) FROM users") #con count() lo que hacemos es devolver el numero de filas de users
        check = cursor.fetchone() #con fetchone devolvemos el registro que se encontro en la base de datos
        filas_bd = check[0] #se asigna este valor a filas_bd
	#print(filas_bd)
        file = open('ultimos_usuarios_final.txt','r') #se abre el archivo final de usuarios con sus ip 
        #Se recorre linea por linea el archivo y se vuelve a quitar usuario y direccion 
        for line in file:
                i=0
                while 1:
                        if(line[i] == '\t'):
                                break
                        i+=1
                usuario = line[:i]
                i+=1
                addr = line[i:]
                i=0
                while 1:
                        if(addr[i] == '\n'):
                                break
                        i+=1
                direccion_ip = addr[:i]
                #El recorrido y comparacion se hace gracias a fetchall
                for k in range(0, filas_bd):# recorremos la base de datos si coinciden el usuario e ip con los almacenados del archivo final
                        band = 0 #ceramos esta variable, porque si vale 0 es que no coincide, y 1 coincide
                        c = k + 1 #la variable c, es el id de la fila de la BD, en cada vuelta incrementamos para recorrer el archivo
                        cursor.execute("SELECT * FROM users where id = %d " % c ) #apuntamos en la fila donde el id es igual a c  
                        variable= cursor.fetchone() #con fetchone devolvemos el registro que se encontro en la BD de la fila que apunta el id y lo guardamos en variable
                        if(usuario == variable[1] and direccion_ip == variable[2]): #Hacemos la comparacion que si el usuario del archivo final es el mismo que se encontro en la BD y lo mismo con la ip
                                band = 1 #son iguales
                if(band == 0): #Entonces si no coinciden el usuario o ip, se realizara la alarma
                        contrasena_random = ''.join([random.choice(string.ascii_letters + string.digits) for n in xrange(10)]) #creamos una contraseña random con random.choice para cambiar al usuario que no coincide
                        #os.system('echo "' + usuario +':'+ contrasena_random + '" | chpasswd') #cambiamos la contrasena del usuario con chpasswd
                        mensaje = 'Se detecto que un usuario y su ip no estan registrados en la base de datos:\n' + usuario + '  [' + direccion_ip + ']' + '\n' + 'se ha procedido a cambiar la pass del usuario\n La nueva pass es:'  + contrasena_random  #mensaje del cuerpo que sera enviado a nuestro correo y registrado en log
                        fecha = time.strftime("%d/%m/%Y")#guardamos fecha y hora con strftime para usar en los mensajes del correo y en el log
                        hora = time.strftime("%H:%M:%S")
                        correo = fecha + ' ====== ' + hora + '\n' + mensaje + '\n\n' #cuerpo completo del mensaje
                        os.system("openssl enc -aes-256-cbc -d -in pass_correo.txt.encrypted -out pass_correo.txt -k ADMINISTRATOR") #se procedera a desencriptar la contraseña de nuestro correo para luego realizar el envio de alerta con smtplib
                        pass_file = open("pass_correo.txt") #guardamos la contraseña en pass_file para luego esta ser reemplazada en otra variable y esta pasar de parametro en el smtplib
                        input_pass_file = pass_file.read().replace('\n','')
                        pass_file.close() #cerramos archivo
                        os.system("rm -rf pass_correo.txt") #siempre eliminamos la contraseña del directorio para que no haya ninguna contraseña a la vista
                        msg['Subject'] = "SE HA DETECTADO UNA ALARMA EN EL HIPS" #cabecera del correo
                        msg.attach(MIMEText(correo, 'plain')) #cuerpo del mensaje
                        server = smtplib.SMTP('smtp.gmail.com: 587') #configuramos los parametros para el envio con smtp
                        server.starttls()
                        server.login(msg['From'], input_pass_file)
                        server.sendmail(msg['From'], msg['To'], msg.as_string()) #se realiza la ejecucion de envio
                        server.quit()
                        mensaje_usuarios = 'echo "\nSe detecto un usuario y su ip no esta registrado en la base de datos, verificar correo para mas informacion\n" ' #Se avisa por terminal que ocurrio algo!
			mensaje_usuarios1 = 'Se detecto un usuario y su ip no esta registrado en la base de datos, verificar correo para mas informacion' #Se avisa por terminal que ocurrio algo!
                        entrada_alarma = fecha + ' ====== ' + hora + '\n' + mensaje_usuarios1 + '\n\n' #cuerpo completo del mensaje
                        os.system(mensaje_usuarios)
                        archivo = open('/var/log/hids/prevencionhids.log', 'a') #registramos en el hids/alarmas.log
                        archivo.write(correo)
                        archivo.close()#cerramos archivo.
                        archivo1 = open('/var/log/hids/alarmashids.log', 'a') #registramos en el hids/alarmas.log
                        archivo1.write(entrada_alarma)
                        archivo1.close()#cerramos archivo.
                        web = open("/home/fabrizio/Escritorio/hidss/HIPS2020/hips2020/hips2020/2.txt", "a")
                        web.write(str((entrada_alarma)))
                        web.close()

        file.close()

# comprobar_ultimos_usuarios ()

#!/usr/bin/python
# -*- coding: utf-8 -*-
# coding=<salt>
from os import listdir
import md5
import os
import hashlib
import string 
import psycopg2 as pgDB
#Crearemos un nuevo .py en donde pondremos las variables globales, las constantes y la importacion de diferentes estructuras
os.system("openssl enc -aes-256-cbc -d -in passbd.txt.encrypted -out passbd.txt -k ADMINISTRATOR")
archivoPassword = open("passbd.txt")
input_pass=archivoPassword.read().replace('\n','') #Abrimos el archivo que contiene la contraseña para hacer la conexion con la base y datos y tratar de cargar lo que necesitamos por psycopg
archivoPassword.close()
os.system("rm -rf passbd.txt")

pgDB_conexion = pgDB.connect(user='root', password=input_pass, dbname='hids2020')
cursor = pgDB_conexion.cursor()
cursor.execute("DROP TABLE IF EXISTS archivos_Con_Firma_md5")
cursor.execute("CREATE TABLE archivos_Con_Firma_md5(id SERIAl, ArchivoDireccion  VARCHAR(100), md5sum VARCHAR(100) )") # direcciones de archivos que se tienen que guardar y ser verificadas
lista_de_camineros = ['/etc/passwd','/etc/shadow']

def CargarArchivos (lista_de_camineros):
    DirectoriosListas =[] # Lista que usaremos para extraer la firma MD5 de cada archivo
    for ruta in lista_de_camineros:          # Vemos si la ruta o caminero es  archivo o una carpeta.               
        if os.path.isdir(ruta):                    
           DirectoriosListas_aux = os.listdir(ruta) # Si es una carpeta, tiene listo un vector con la direccion del archivo que vamos a analizar y utilioza         
           for ListaElemento in DirectoriosListas_aux:
                DirectoriosListas.append(ruta + '/' + ListaElemento )
        else:                                       
           DirectoriosListas = [ruta]                       

        for direccion in DirectoriosListas:                 
            archivo_temp = hashlib.md5((open(direccion)).read())
            sum_md5 = str(archivo_temp.hexdigest()) # Asi tenemos la sum_md5 de cada archivo 
            try :
                # INSERTAMOS LAS FIRMAS EN LA TABLA 
                 cursor.execute("INSERT INTO archivos_Con_Firma_md5(ArchivoDireccion, md5sum) VALUES (%s,%s)",(direccion, sum_md5))
            except pgDB.Error as error:
                print("Error: {}".format(error))
            pgDB_conexion.commit()

CargarArchivos(lista_de_camineros)

def cargar_usuarios():
    try:
        cursor = pgDB_conexion.cursor()   # conectamos 
        cursor.execute("DROP TABLE IF EXISTS users")# se borrara la tabla si existe
         
        cursor.execute("CREATE TABLE users(id SERIAL, usr VARCHAR(30), addr VARCHAR(30), email VARCHAR(30), pass VARCHAR(30))")  # se crea la table user en donde se agregaran los usuarios, ips, email, pass
    except:
        print "Error en acceder a la BD"
        return

    os.system("openssl enc -aes-256-cbc -d -in lista_usuarios.txt.encrypted -out lista_usuarios.txt -k ADMINISTRATOR")
    Archivo=open('lista_usuarios.txt','r') #Abre el archivo que contiene la lista de usuarios permitidos    

    lineas = Archivo.read().split(os.linesep)

    for aux in lineas[:-1]:
        if(aux != ''):
            vec = aux.split(' ') #Agregamos el entrecomillado o parseado en un vector en los cuales identifican cada uno de los datos a ser insertado
            try:        
                cursor.execute("INSERT INTO users ( usr, addr, email, pass) VALUES (%s,%s,%s,%s)",(vec[0],vec[1],vec[2],vec[3][:len(vec[3])]) )  #Insertamos los datos 
		#print(vec[3][:len(vec[3])])
		#print(vec[3])
            except pgDB.Error as error:
                print("Error: {}".format(error))
            pgDB_conexion.commit()
    Archivo.close()
    os.system("rm -rf lista_usuarios.txt")#eliminamos siempre el txt 

cargar_usuarios()

def CargarCrontab():
    try:
        cursor = pgDB_conexion.cursor()                     
        cursor.execute("DROP TABLE IF EXISTS Crontab")     # Borramos la tabla si es que ya existe
        cursor.execute("CREATE TABLE Crontab(id SERIAL, NombreCrontab VARCHAR(60) )") #Aca crearemos la tabla con los nombres de un archivo .txt 
    except:
        print "Error al entrar ala BD"
        return
    #Para no dejar expuesto las contraseñas del correo o de la bases de datos, encriptamos con con openssl el archivo txt. entonces aca desencriptamos para usar el txt
    # abrimos el archivo que contiene los nombres de los Snifferers
    os.system("openssl enc -aes-256-cbc -d -in tareas_Crontab_guardadas.txt.encrypted -out tareas_Crontab_guardadas.txt -k ADMINISTRATOR")
    Archivo1 = open('tareas_Crontab_guardadas.txt','r')    

    for line in Archivo1:                    
        tam=len(line)-1 #tomamos el tam
        Crontab=line[:tam]
        try:
            cursor.execute("INSERT INTO Crontab ( NombreCrontab) VALUES ( %s) " ,(Crontab, ) )#insertamos en la BD los nombres de los Sniffer(ethereal, tcpdump)
        except pgDB.Error as error:
            print("Error: {}".format(error))
        pgDB_conexion.commit()

    Archivo1.close()

CargarCrontab()

def CargarlosLosSniffers():
    try:
        cursor = pgDB_conexion.cursor()                     
        cursor.execute("DROP TABLE IF EXISTS Sniffers")     # Borramos la tabla si es que ya existe
        cursor.execute("CREATE TABLE Sniffers(id SERIAL, NombreSniffer VARCHAR(32) )") #Aca crearemos la tabla con los nombres de un archivo .txt 
    except:
        print "Error al entrar ala BD"
        return
    #Para no dejar expuesto las contraseñas del correo o de la bases de datos, encriptamos con con openssl el archivo txt. entonces aca desencriptamos para usar el txt
    os.system("openssl enc -aes-256-cbc -d -in lista_sniffers.txt.encrypted -out lista_sniffers.txt -k ADMINISTRATOR")
    # abrimos el archivo que contiene los nombres de los Snifferers
    Archivo = open('lista_sniffers.txt','r')    

    for line in Archivo:                    
        tam=len(line)-1 #tomamos el tam
        Sniffer=line[:tam]
        try:
            cursor.execute("INSERT INTO Sniffers ( NombreSniffer) VALUES ( %s) " ,(Sniffer, ) )#insertamos en la BD los nombres de los Sniffer(ethereal, tcpdump)
        except pgDB.Error as error:
            print("Error: {}".format(error))
        pgDB_conexion.commit()

    Archivo.close()

    pgDB_conexion.close()


CargarlosLosSniffers()



pgDB_conexion.close()

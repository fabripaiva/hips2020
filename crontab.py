#!/usr/bin/python
# -*- coding: utf-8 -*-
# coding=<salt>
from ImportsyConexiones import *
def crontab():
        os.system("rm -rf 5.txt")
        cursor.execute("SELECT NombreCrontab FROM Crontab") #apuntamos en la direccion donde estan los nombres de las tareas crontab 
        lista_temporal = cursor.fetchall() #con fetchall devolvemos una lista con los registros de los nombres de las tareas crontab  para su proxima comparacion
        crontab_lista = 0  #preparamos una variable
        for temp in lista_temporal: # recorremos la lista las tareas crontab  que guardamos en una lista temporal
                crontab_lista = temp #agregamos elemento a nuestra lista, este caso el nombre del las tareas crontab en nuestra otra lista que creamos
        os.system("crontab -l > tareas_check.txt") #creamos un archivo donde se guardaran todos las tareas crontab recientes en el sistema
        #print("sadasd")
        #se abre el archivo creado anteriormente
        procesos_crontab = open('tareas_check.txt','r') #abrimos el archivo y lo guardamos en procesos_crontab para su recorrido
        procesos_crontab1 = procesos_crontab.readlines()
        print(crontab_lista)
        for fila in procesos_crontab1: #recorremos lo que contenia el archivo
                #print(fila)
                for i in crontab_lista: #recorremos nuestra otra lista en donde se guardaron los nombres de las tareas crontab
                        #print(i)
                        # Ahora hacemos la comparacion y si coincide matamos el proceso del sniffers y mandamos la alerta por correo y al hids
                        if (i != fila):# recorremos los nombres que pusimos en i y comparamos con lo que habia en el archivo
                                fecha = time.strftime("%d/%m/%Y") #guardamos fecha y hora con strftime para usar en los mensajes del correo y en el log
                                hora = time.strftime("%H:%M:%S")
                                mensaje = 'Se ha detectado una tarea no programada en crontab con el nombre de  ' + fila #mensaje para el correo y el registro en logs/hids
                                mensaje_completo = fecha + ' ====== ' + hora + '\n' + mensaje + '\n\n'   #Mensaje completo que sera enviado al correo y al /var/hids
                                os.system("openssl enc -aes-256-cbc -d -in pass_correo.txt.encrypted -out pass_correo.txt -k ADMINISTRATOR")   #se procedera a desencriptar la contraseña de nuestro correo para luego realizar el envio de alerta con smtplib
                                password_correo = open("pass_correo.txt") #guardamos la contraseña en pass_file para luego esta ser reemplazada en otra variable y esta pasar de parametro en el smtplib
                                input_password_correo = password_correo.read().replace('\n','')
                                password_correo.close()
                                msg['Subject'] = "SE HA DETECTADO UNA ALARMA EN EL HIPS"   #cabecera del correo
                                msg.attach(MIMEText(mensaje_completo, 'plain'))  #cuerpo del Mensaje
                                server = smtplib.SMTP('smtp.gmail.com: 587')     #configuramos los parametros para el envio con smtp
                                server.starttls()
                                server.login(msg['From'], input_password_correo)
                                server.sendmail(msg['From'], msg['To'], msg.as_string())     #se realiza la ejecucion de envio
                                server.quit()
                                archivo = open('/var/log/hids/alarmashids.log', 'a') #mensaje para la alarma del registro del log
                                archivo.write(str((mensaje_completo)))
                                mensaje_term = 'echo "\nSe ha detectado una tarea no programada en crontab, porfavor verifique su correo para mas info\n" ' #avisamos en la terminal
                                os.system(mensaje_term)
                                os.system("rm -rf pass_correo.txt") #siempre eliminamos la contraseña del directorio para que no haya ninguna contraseña a la vista
                                archivo.close()
                                #os.system("crontab -r root") #eliminamos la tarea como prevencion
                                #os.system("crontab -r ") #eliminamos la tarea como prevencion
                                mensaje1 = 'Se ha procedido a eliminar la tarea del crontab' + fila + 'con crontab -r'#mensaje para el correo y el registro en logs/hids
                                mensaje_completo1 = fecha + ' ====== ' + hora + '\n' + mensaje1 + '\n\n'   #Mensaje completo que sera enviado al correo y al /var/hids
                                archivo1 = open('/var/log/hids/prevencionhids.log', 'a') #mensaje para la alarma del registro del log
                                archivo1.write(str((mensaje_completo1)))
                                archivo1.close()
                                os.system("rm -rf tareas_Crontab_guardadas.txt tareas_check.txt") #eliminamos los archivos creados
                                web = open("/home/fabrizio/Escritorio/hidss/HIPS2020/hips2020/hips2020/5.txt", "a")
                                web.write(str((mensaje_completo)))
                                web.close()

        procesos_crontab.close()
        os.system("rm -rf tareas_check.txt")

#crontab()

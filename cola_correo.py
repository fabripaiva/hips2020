#!/usr/bin/python
# -*- coding: utf-8 -*-
# coding=<salt>
from ImportsyConexiones import *

def cola_correo():
        os.system("rm -rf 4.txt") 
        archivo_mails_recibidos = open('/var/spool/mail_recibidos.txt','r') #abrimos el archivo en donde se almacenan los correos, en kali 2020 es /var/spool
        cant = archivo_mails_recibidos.readlines() #lo leemos y guardamos en la variable cant
        cantidad = len(cant) #con len devolvemos el numero de elementos que existe en cant
        if((cantidad) > 20): #comparamos con la cantidad permitida
                cantidad = str(cantidad) #convertimos el valor en una cadena con str
                mensaje ='La cola de correo supero la cantidad establecida permidite, Actualimente hay ' + cantidad + ' correos en cola' #mensaje para el cuerpo del correo que se enviara y se registrara en alarmashids.logs
                fecha = time.strftime("%d/%m/%Y")#guardamos fecha y hora con strftime para usar en los mensajes del correo y en el log
                hora = time.strftime("%H:%M:%S")
                entrada = fecha + ' ====== ' + hora + '\n' + mensaje + '\n\n' #Mensaje completo que sera enviado al correo y al /var/hids
                os.system("openssl enc -aes-256-cbc -d -in pass_correo.txt.encrypted -out pass_correo.txt -k ADMINISTRATOR") #se procedera a desencriptar la contraseña de nuestro correo para luego realizar el envio de alerta con smtplib
                password_correo = open("pass_correo.txt") #guardamos la contraseña en password_correo para luego esta ser reemplazada en otra variable y esta pasar de parametro en el smtplib
                input_password_correo = password_correo.read().replace('\n','')
                password_correo.close()
                os.system("rm -rf pass_correo.txt") #siempre eliminamos la contraseña del directorio para que no haya ninguna contraseña a la vista
                msg['Subject'] = "SE HA DETECTADO UNA ALARMA EN EL HIPS" #cabecera del correo
                msg.attach(MIMEText(entrada, 'plain')) #cuerpo del Mensaje
                server = smtplib.SMTP('smtp.gmail.com: 587') #configuramos los parametros para el envio con smtp
                server.starttls()
                server.login(msg['From'], input_password_correo)
                server.sendmail(msg['From'], msg['To'], msg.as_string())#se realiza la ejecucion de envio
                server.quit()
                archivo = open('/var/log/hids/alarmashids.log', 'a') #procedemos abrir el archivo para escribir la alarma en logs/hids , para que quede guardado un registro en la bitacora
                archivo.write(entrada) #escribimos en el archivo la entrada
                archivo.close() #cerramos el archivo
                mensaje_pass = 'echo "\nSe detecto que la cola de correo esta lleno y sobrepaso su limite, porfavor revise su gmail.\n" ' #avisamos en la terminal
                os.system(mensaje_pass)
                web = open("/home/fabrizio/Escritorio/hidss/HIPS2020/hips2020/hips2020/4.txt", "a")
                web.write(str((entrada)))
                web.close()

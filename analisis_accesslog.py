#!/usr/bin/python
# -*- coding: utf-8 -*-
# coding=<salt>
from ImportsyConexiones import *

def analisis_access_log():
        os.system("rm -rf 1.txt") 
        archivo = open('/home/fabrizio/Descargas/access_log').read() #abrimos el archivo con los intentors de access log que el profesor nos envio
        archivo = archivo.split(os.linesep) #separamos cada linea cuando se encuentre un salto de linea
        ipsPost = [] #guardaremos las ip's en donde se encuentre POST, error 404 y acceso a TMP 
        ips404 = []#guardaremos las ip's en donde se encuentre POST, error 404 y acceso a TMP
        ipsTmp = []#guardaremos las ip's en donde se encuentre POST, error 404 y acceso a TMP
        #get hace una peticion o post a tu servidor
        for linea in archivo[:-2]:#recorreremos el archivo
                linea_split = linea.split(' ')  #en linea split guarda una lista, y cada elemento es separado por espacio 
                caso_HTTP = linea_split[5]  #Extraemos el metodo http utilizado
                if caso_HTTP == 'POST':#verificamos
                        if 'login' in linea_split[6]:  #Si es un post luego verificaremos que sea un LOGIN
                                fecha = (linea_split[3].split(':'))[0] #guardamos la fecha para verificar la cantidad de veces
                                #print(fecha)
                                ipsPost.append((fecha,linea_split[0]))#agregamos los elementos a la lista usuario_cont
                                contadoripsPost = [[x,ipsPost.count(x)] for x in set(ipsPost)] #Variable para almacenar cuantas ips que en su linea aparecia POST
                                for verif in contadoripsPost: #asignamos la cantidad a verif para realizar la verificacion
                                #Si una IP supera N intentos de acceso, se realizara el aviso, utilizaremos la misma sintaxis que el cuerpo de arriba
                                    if verif[1] > N:                 
                                        fecha = time.strftime("%d/%m/%Y")
                                        hora = time.strftime("%H:%M:%S")
                                        Mensaje_Correo = 'Se bloqueo una ip por superar intentos de acceso en HTTPD'
                                        entrada_access_log_http = fecha + ' ====== ' + hora + '\n' + Mensaje_Correo + '----> ' + 'IP:' + verif[0][1] + '\n\n'
                                        archivo = open('/var/log/hids/prevencionhids.log', 'a')
                                        #misma sintaxis que los otros cuerpos del codigo
                                        archivo.write(str((entrada_access_log_http)))
                                        os.system("openssl enc -aes-256-cbc -d -in pass_correo.txt.encrypted -out pass_correo.txt -k ADMINISTRATOR")
                                        pass_file = open("pass_correo.txt")#guardamos la contraseña en pass_file para luego esta ser reemplazada en otra variable y esta pasar de parametro en el smtplib
                                        input_pass_correo =pass_file.read().replace('\n','')
                                        pass_file.close()
                                        os.system("rm -rf pass_correo.txt")
                                        msg['Subject'] = "SE HA DETECTADO UNA ALARMA EN EL HIPS"
                                        msg.attach(MIMEText(entrada_access_log_http, 'plain'))
                                        server = smtplib.SMTP('smtp.gmail.com: 587')
                                        server.starttls()
                                        server.login(msg['From'], input_pass_correo)
                                        server.sendmail(msg['From'], msg['To'], msg.as_string())
                                        server.quit()
                                        Mensaje_Correo_tmp = 'echo "\nSe detecto que una ip supero los intentos de acceso, porfavor ver el correo\n" ' #avisamos en lña terminal
                                        mensaje_alarma1 = 'Se detecto que una ip supero los intentos de acceso, porfavor ver el correo'
                                        entrada_alarma1 = fecha + ' ====== ' + hora + '\n' + mensaje_alarma1 + '\n\n'
                                        archivo2 = open('/var/log/hids/alarmashids.log', 'a') #abrimos el archivo txt para escribir el registro
                                        archivo2.write(str((entrada_alarma1)))
                                        archivo2.close()
                                        os.system(Mensaje_Correo_tmp)
                                        archivo.close()
                                        os.system('iptables -A INPUT -s ' + verif[0][1] + ' -j DROP') #Se bloquea el trafico de la ip con iptables
                                        #print(verif[0][1])
                                        #print(verif[1])
                                        ip_bloqueadas.append(verif[0][1])
                                        web = open("/home/fabrizio/Escritorio/hidss/HIPS2020/hips2020/hips2020/1.txt", "a")
                                        web.write(str((mensaje_alarma1)))
                                        web.close()
                                        archivo.close()
                else:
                    if '404' in linea_split[8]:     #Si no es un POST verificaremos los GETS con url que no son validas
                        fecha = (linea_split[3].split(':'))[0]
                        ips404.append((fecha,linea_split[0]))
                        linea_tmp = linea.split('"',1)[1] #guarda despues de la primera comilla para que se convierta en una lista
                        #print(linea_tmp)
                        if '/tmp/' in linea_tmp: #Aca verificaremos si no hubo intentos de acceso en la carpeta /tmp
                            ipsTmp.append((fecha,linea_split[0]))
                            fecha = time.strftime("%d/%m/%Y")#guardamos la fecha y hora para la posterior configuracion
                            hora = time.strftime("%H:%M:%S")
                            Mensaje_Correo = 'Se bloqueo un ip por intentar acceder y ejecutar en el directorio /TMP' #Mensaje_Correo para el cuerpo del correo y para el log
                            entrada_correo_tmp = fecha + ' ====== ' + hora + '\n' +    Mensaje_Correo + '  ---->  ' + 'IP:' + linea_split[0] + '\n\n'
                            archivo = open('/var/log/hids/prevencionhids.log', 'a') #enviamos un informe para que quede en la bitacora prevencion_hids.log
                            archivo.write(str((entrada_correo_tmp)))
                            #En la carpeta tendremos un archivo encriptado con openssl, esto para que no haya ninguna contraseña visible entonces cuando se procedera a desencriptar la contraseña de nuestro correo para luego realizar el envio de alerta con smtplib
                            os.system("openssl enc -aes-256-cbc -d -in pass_correo.txt.encrypted -out pass_correo.txt -k ADMINISTRATOR")
                            pass_file = open("pass_correo.txt")#guardamos la contraseña en pass_file para luego esta ser reemplazada en otra variable y esta pasar de parametro en el smtplib
                            input_pass_correo =pass_file.read().replace('\n','')
                            pass_file.close()
                            os.system("rm -rf pass_correo.txt")#siempre eliminamos la contraseña del directorio para que no haya ninguna contraseña a la vista
                            msg['Subject'] = "SE HA DETECTADO UNA ALARMA EN EL HIPS"   #cabecera del correo
                            msg.attach(MIMEText(entrada_correo_tmp, 'plain')) #cuerpo del Mensaje_Correo 
                            server = smtplib.SMTP('smtp.gmail.com: 587')#configuramos los parametros para el envio con smtp
                            server.starttls()
                            server.login(msg['From'], input_pass_correo)
                            server.sendmail(msg['From'], msg['To'], msg.as_string()) #se realiza la ejecucion de envio
                            server.quit()
                            Mensaje_Correo_tmp = 'echo "\nHubo in intento de acceso en el directorio /TMP!!!!! Porfavor revisar correo \n" ' #Se avisa por terminal que ocurrio algo!
                            mensaje_tmp_alarma = 'Hubo in intento de acceso en el directorio /TMP!!!!! Porfavor revisar correo'
                            entrada_alarma = fecha + ' ====== ' + hora + '\n' +   mensaje_tmp_alarma + '\n\n' #procedemos a registrar la alarma en el log
                            archivo1 = open('/var/log/hids/alarmashids.log', 'a') #abrimos el archivo txt para escribir el registro
                            archivo1.write(str((entrada_alarma)))  
                            archivo1.close()
                            os.system(Mensaje_Correo_tmp)
                            archivo.close()
                            os.system('iptables -A INPUT -s ' + linea_split[0] + ' -j DROP') #Se bloquea el trafico de la ip con iptables 
                            ip_bloqueadas.append(linea_split[0])
                            web = open("/home/fabrizio/Escritorio/hidss/HIPS2020/hips2020/hips2020/1.txt", "a")
                            web.write(str((entrada_alarma)))
                            web.close()
	                       #Creamos un duo de variables {ip, cantidad de intentos de acceso}
                            contadorips404 = [[x,ips404.count(x)] for x in set(ips404)]    #Variable para almacenar cuantas ips que en su linea aparecia 404
	                       #print(contadorips404)

        

        #Si un usuario supera N intentos de acceso a una pagina inexistente, se realizara el aviso
                            for verif in contadorips404: #asignamos la cantidad a verif para realizar la verificacion
                                if verif[1] > N:         
                                    fecha = time.strftime("%d/%m/%Y")
                                    hora = time.strftime("%H:%M:%S")
                                    Mensaje_Correo = 'Se bloqueo una ip por superar el numeros de intentos a una pagina que no existe'
                                    entrada_access_404 = fecha +' ====== ' + hora + '\n'+ Mensaje_Correo+ '---->' + 'IP:' + verif[0][1] + '\n\n'
                                    archivo=open('/var/log/hids/prevencionhids.log', 'a')
                                    #misma sintaxis que los otros cuerpos del codigo
                                    archivo.write(str((entrada_access_404)))
                                    os.system("openssl enc -aes-256-cbc -d -in pass_correo.txt.encrypted -out pass_correo.txt -k ADMINISTRATOR")
                                    pass_file = open("pass_correo.txt") #guardamos la contraseña en pass_file para luego esta ser reemplazada en otra variable y esta pasar de parametro en el smtplib
                                    input_pass_correo =pass_file.read().replace('\n','')
                                    pass_file.close()
                                    os.system("rm -rf pass_correo.txt")
                                    msg['Subject'] = "SE HA DETECTADO UNA ALARMA EN EL HIPS!"
                                    msg.attach(MIMEText(entrada_access_404, 'plain'))
                                    server = smtplib.SMTP('smtp.gmail.com: 587')
                                    server.starttls()
                                    server.login(msg['From'], input_pass_correo)
                                    server.sendmail(msg['From'], msg['To'], msg.as_string())
                                    server.quit()
                                    Mensaje_Correo_404 = 'echo "\nSe bloqueo una ip por superar el numero de acceso a paginas que no existen, porfavor revisar el correo\n" ' #avisamos en la terminal
                                    mensaje_alarma2 = 'Se bloqueo una ip por superar el numero de acceso a paginas que no existen, porfavor revisar el correo'
                                    entrada_alarma2 = fecha + ' ====== ' + hora + '\n' + mensaje_alarma2 + '\n\n'
                                    archivo2 = open('/var/log/hids/alarmashids.log', 'a') #abrimos el archivo txt para escribir el registro
                                    archivo2.write(str((entrada_alarma2)))
                                    archivo2.close()
                                    os.system(Mensaje_Correo_404)
                                    archivo.close()
                                    os.system('iptables -A INPUT -s ' + verif[0][1] + ' -j DROP')#Se bloquea el trafico de la ip con iptables
                                    ip_bloqueadas.append(verif[0][1])
                                    web = open("/home/fabrizio/Escritorio/hidss/HIPS2020/hips2020/hips2020/1.txt", "a")
                                    web.write(str((entrada_alarma2)))
                                    web.close()

#!/usr/bin/python
# -*- coding: utf-8 -*-
# coding=<salt>
#Este archivo se asignara a la funcion de comprobar si existe algun ejecutable en la carpeta /tmp
from ImportsyConexiones import *
def analisis_tmp():
        os.system("rm -rf 10.txt")
        for dirpath,_,filenames in os.walk('/tmp/'): #asignamos el caminero del directorio tmp 
                for f in filenames:  # si se encuentra un archivo ejecutable se genera la alarma correspondiente, se pondra en cuarentena o la eliminacion
                        if (('.py' in f ) or ('.sh' in f ) or ('.exe' in f) or  ('.deb' in f )): #verifica las extensiones
                                band=1
                                fecha = time.strftime("%d/%m/%Y") #guardamos la fecha y hora para la posterior configuracion 
                                hora = time.strftime("%H:%M:%S")
                                mensaje = "Se ha detectado un archivo ejecutable, Se ejecutara su envio a cuarentena."
                                entrada_ejecutable = fecha + ' ====== ' + hora + '\n' + mensaje + ' ---->  ' + f + '\n\n'
                                archivo=open('/var/log/hids/prevencionhids.log', 'a')
                                os.system("chmod 400 " + "/tmp/" + f) #Quitamos todos los permisos del archivo y ponemos solo lectura para el duenho
                                os.system("mv "+ "/tmp/" + f + " /home/Cuarentena") #Ponemos el archivo en carpeta oculta de cuarentena
                                print("\nEl archivo "+ f +" fue puesto en cuarentena. \n")
                                archivo.write(str((entrada_ejecutable)))
			       #se procedera a desencriptar la contraseña de nuestro correo para luego realizar el envio de alerta con smtplib
                                #En la carpeta tendremos un archivo encriptado con openssl, esto para que no haya ninguna contraseña visible 					       desencripte con el comando de abajo luego de usar se eliminara
                                os.system("openssl enc -aes-256-cbc -d -in pass_correo.txt.encrypted -out pass_correo.txt -k ADMINISTRATOR")
                                pass_correo = open("pass_correo.txt")
                                datapass_correo = pass_correo.read().replace('\n','') #se copia a esta variable para poner de parametro en la configuracion
                                pass_correo.close() # se cierra archivo
                                os.system("rm -rf pass_correo.txt") #siempre eliminamos la contraseña del directorio para que no haya ninguna contraseña a la vista
                                msg['Subject'] = "SE HA DETECTADO UNA ALARMA EN EL HIPS" #cabecera del mensaje
                                msg.attach(MIMEText(entrada_ejecutable, 'plain')) #cuerpo del mensaje
		               #configuramos los parametros para el envio con smtp
                                server = smtplib.SMTP('smtp.gmail.com: 587')
                                server.starttls()
                                server.login(msg['From'], datapass_correo)
                                server.sendmail(msg['From'], msg['To'], msg.as_string()) #se realiza la ejecucion de envio
                                server.quit()
                                mensaje_ejecutable = 'echo "Se encontro un ejecutable, ver correo para mas informacion" '#Se avisa por terminal que ocurrio algo!
                                mensaje_alarma = 'Se encontro un ejecutable, ver correo para mas informacion'
                                entrada_alarma = fecha + ' ====== ' + hora + '\n' + mensaje_alarma + '\n\n\n'
                                archivo1 = open('/var/log/hids/alarmashids.log', 'a') #abrimos el archivo txt para escribir el registro
                                archivo1.write(str((entrada_alarma)))  
                                archivo1.close()
                                os.system(mensaje_ejecutable)
                                archivo.close()
                                web = open("/home/fabrizio/Escritorio/hidss/HIPS2020/hips2020/hips2020/10.txt", "a")
                                web.write(str((entrada_alarma)))
                                web.close()
        if band != 1:    
                print("la carpeta esta limpia")
        return 


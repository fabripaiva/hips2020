# -*- coding: utf-8 -*-
# coding=<salt>
from flask import Flask, render_template, request, session, redirect,url_for,flash
from flask_login import current_user, LoginManager, login_required, login_user, logout_user #render template es para "pintar" nuestro html usando el patron mcd, request para recibir peticiones, GET etcs
import os
#from flask_mysqldb import MySQL
import bcrypt

from forms import LoginForm

from models import UserData, UserModel, get_user

from flask_bootstrap import Bootstrap

app = Flask(__name__) #instanciamos con flask, app sera una aplicacion flask
bootstrap = Bootstrap(app)

app.secret_key = "secret_key"
"""
mysql = MySQL(app)

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'dbHids'
"""
#instanciador de login manager para flask
login_manager = LoginManager()
login_manager.login_view = '/login'
login_manager.init_app(app)

BASE_URL = "/home/fabrizio/Escritorio/hidss/HIPS2020/hips2020/hips2020" #variable en donde estan todos nuestros archivos

@login_manager.user_loader #se llama cada vez que se quiera iniciar sesion
#cuando iniciamos login llamamos a este query, este query trae ese usuario del archivo y si no existe retorna un none
def load_user(user_id):
    return UserModel.query(user_id)
#Tenemos dos metodos, el get o post, el get es para que te muestre mi formulario, y el post para que envie los datos de ese formulario
@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()  #formulario de login
    if form.validate_on_submit():  #veriricaremos si el form esta correcto
        username = form.username.data #en el form vemos el username para pasarle de parametro aca y lo guardamos en la variable username
        password = form.password.data #en el form vemos el password para pasarle de parametro aca y lo guardamos en la variable username

        user_doc = get_user(username) #verifica si existe el usuario

        if user_doc is not None:    #verificamos si existe ese usuario en nuestro archivo y ahi le pasamoos su contraseña 
            #print("hola " + user_doc.password)
            password_from_db = user_doc.password   #el pass que trajo userdoc se guarda , password_from_db es la pass que se trajo del archivo
            if password == password_from_db: #si el pass es el pass que trajo del form, que seria lo que puso el ususario
                user_data = UserData(username, password)
                user = UserModel(user_data)
                login_user(user) #es indicarle que loguee con ese usuario
                flash('Bienvenido')  
                return redirect(url_for('home'))  #si se logueo se va a home
            else:
                flash('La informacion no coincide') #si no avisamos que no coincide
        else:
                flash('El usuario no existe') #si el usuario del formulario no existe en el archivo, pues no existe

    return render_template('login.html', login_form=form) #si es que no se loguea se ejecuta el login, si el usuario puso algo que no existe

@app.route('/logout')
@login_required    #Sirve para cerrar sesion
def logout():
    logout_user()
    flash('Regresa pronto')
    return redirect(url_for('login')) #si no estas registrado te redirige al login

@app.route('/') #el decorador app.route es nuestra ruta URI o URL de la pagina de nuestra url, primero va nuestro localhost que es nuestro dominio y luego del dominio viene la URI o URL;esa ruta va en ese entrecomillado 
@login_required
def home(): #luego definimos que queremos que venga despues de nuestra url
    return render_template('home.html', username=current_user.id)  #retorna un render template que sirve para pintar un html

@app.route('/about') #/about estan los integrantes del grupo
@login_required
def about():
    return render_template('about.html') #retorna igualmente un rendertemplate 

#esta es la parte mas importante
@app.route('/<id>')  #recibimos un parametro ID, 
def lista_resultado(id):
    #En nuestra carpeta contiene 12 archivos opcion1.py ... opcion12.py , en estos archivos python estaran las funciones especificas a cada analisis
    # entonces con os.system ejecutamos el archivo de acuerdo a que parametro se paso al ID, si es 3 entonces se ejecuta opcion3.py
    os.system(f"python2.7 opcion{id}.py")
    #en tittle se guarda el nombre de la opcion por ejemplo: Comprobar accesslog
    title = request.args.get('titulo') #el request.args son los parametros que recibimos en nuestra url, el .get es que parametro podemos recibir especificamente
    # f es format string, f es una funcion de python que tu le mandas un string, y todo lo que esta entrecorchete el lo toma como una variable y esa variable debe estar en tu funcion
    #entonces lo que hace el format string agarrar BASE_URL e inyecta en ese lugar en donde le decimos, para luego recibir el id.txt
    with open(f'{BASE_URL}/{id}.txt', 'r') as f: #abrimos el archivo en donde nuestro programa debe crear los archivos que se escribe en la salida, Por ejemplo: Se ha detectado un acceso a tmp
	#esas salidas se grabaran en un archivo y luego van hacer abiertas en esta parte para guardarlas en data y pasarla a nuestra pagina html
        data = f.read() #leemos lo que contiene el archivo y lo guardamos en data
        context = {
        'titulo': title, #f'Lista {id}',    #luego tittle se almacena en titulo para pasarle a nuestro HTML
        'data' : data.split('\n')		 #luego hacemos un split en data para separar las cadenas cuando se detecta un espacio y lo guardamos en otra variable DATA para pasarle a nuestro HTML
        }
        #print(data)

    return render_template('data_card.html', **context) #luego llamamos de nuevo a render template, esta vez es un html puede recibir parametros, para que nuestro html pueda ser estatico, basicamente poder recibir parametros

if __name__ == '__main__':   #if name es para iniciar la aplicacion
    app.run()


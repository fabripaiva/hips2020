#!/usr/bin/python
# -*- coding: utf-8 -*-
# coding=<salt>
from ImportsyConexiones import *
def ataque_dns():
        os.system("rm -rf 3.txt") 
        archivo_temporal = open('/home/fabrizio/Descargas/Ataque_DNS_-_tcpdump.txt').read() #primeramente abrimos el archivo en donde contiene los ataques
        contador_ataque_dns = [] #esta sera nuestra lista en donde se agregaran los elementos de una dupla que consistira en la ip del atacador y la ip destino, 
         #entonces gracias a la ip destino que es igual se podra contar cuantas veces se ataco, con eso se creara un lazo de dupla (ip,intentos) de la siguiente manera [[('87.211.5.65.24045', '201.217.5.142.53:'), 12]
        verificador = 0 #una bandera por si haya una discrepancia
        filas = archivo_temporal.split(os.linesep)  #Separamos las cadenas cuando se detecte un salto de linea y lo guardamos en filas
        for linea in filas [:-1]: #recorremos lo que guardamos en filas
                ip_destino = linea.split(' ')[4]  #guardamos las cadenas numero 4 que son las ip destino y lo guardamos en una variable
                ip_atacador = linea.split(' ')[2] #guardamos las cadenas numero 2 que son las ips de los atacantes y los guardamos en una variable
                #ip_destino = ip_destino.replace(":", "")
		#print(ip_destino)
                contador_ataque_dns.append((ip_atacador, ip_destino)) #agregamos los elementos ip_destino e ip_atacador a nuestra lista de contador ataque
        contador_registro_dns = [[x,contador_ataque_dns.count(x)] for x in set(contador_ataque_dns)]   #como dijimos mas arriba definimos una dupla gracias a la ip destino y las ips atacantes y asi
	#se mide cuantas veces se intento acceder, de la sgte manera: [[('87.211.5.65.24045', '201.217.5.142.53:'), 12]
	#print(contador_registro_dns)
        for temp in contador_registro_dns: #Entonces ahora asociamos nuestro contador dupla con temp para realizar la comparacion
		#temp1 porque ahi apunta la cantidad de veces ej: [[('87.211.5.65.24045', '201.217.5.142.53:'), 12] , en este caso 12
                if temp[1] > 14:        #si se registran mas de 14 intentos de acceso por un usuario se trata de DOS
                        fecha = time.strftime("%d/%m/%Y") #guardamos fecha_verif y hora con strftime para usar en los mensajes del correo y en el log
                        hora = time.strftime("%H:%M:%S")
                        mensaje = 'Se ha procedido a bloquear una IP por intento de ataque DDOS' #mensaje para el correo y el registro en logs/hids
			#en ip_dist separamos la ip del atacador en 3 porque en el ip tables no admite mas de 4 cadenas
                        ip_dist = temp[0][0].split('.')[:-1]  #poner parametro [:-1] para que funcione en el ip tables
			#print(ip_temp)
                        Ip = ip_dist[0]+'.'+ip_dist[1]+'.'+ip_dist[2]+'.'+ip_dist[3]  #ip atacador final, sacar el ip_dist[4] tb para el iptables
                        entrada_ataq = fecha + ' ====== ' + hora + '\n' + mensaje + ' ----> ' + 'IP:' + Ip + '\n\n' #Mensaje completo que sera enviado al correo y al /var/hids
                        archivo=open('/var/log/hids/prevencionhids.log', 'a') #abrimos el archivo para escribir /var/log/hids
                        archivo.write(str((entrada_ataq))) #escribimos en el archivo
			#se procedera a desencriptar la contraseña de nuestro correo para luego realizar el envio de alerta con smtplib
                        os.system("openssl enc -aes-256-cbc -d -in pass_correo.txt.encrypted -out pass_correo.txt -k ADMINISTRATOR")
                        pass_file = open("pass_correo.txt") #guardamos la contraseña en pass_file para luego esta ser reemplazada en otra variable y esta pasar de parametro en el smtplib
                        input_pass_file = pass_file.read().replace('\n','')
                        pass_file.close()
                        os.system("rm -rf pass_correo.txt") #siempre eliminamos la contraseña del directorio para que no haya ninguna contraseña a la vista
                        msg['Subject'] = "SE HA DETECTADO UNA ALARMA EN EL HIPS" #cabecera del correo
                        msg.attach(MIMEText(entrada_ataq, 'plain')) #cuerpo del Mensaje_Correo
                        server = smtplib.SMTP('smtp.gmail.com: 587') #configuramos los parametros para el envio con smtp
                        server.starttls()
                        server.login(msg['From'], input_pass_file)
                        server.sendmail(msg['From'], msg['To'], msg.as_string()) #se realiza la ejecucion de envio
                        server.quit()
                        mensaje_dos = 'echo "\nSe ha detectado que una ip sobrepaso los limites de conexion, se podria tratar de un ataque DDOS\n" '
                        mensaje_alarma='Se ha detectado que una ip sobrepaso los limites de conexion, se podria tratar de un ataque DDOS' #mensaje para la alarma del registro del log
                        logs_alarma = fecha + ' ====== ' + hora + '\n' + mensaje_alarma + '\n\n' #cuerpo del mensaje de alarma
                        archivo1 = open('/var/log/hids/alarmashids.log', 'a') #abrimos el archivo txt para escribir el registro
                        archivo1.write(str((logs_alarma)))
                        archivo1.close()
                        os.system(mensaje_dos)
                        os.system('iptables -A INPUT -s ' + Ip + ' -j DROP') # procedemos a bloquear la ip con iptables
                        ip_bloqueadas.append(Ip) #agregamos la ip bloqueada a nuestra lista de ip's bloqueadas
                        archivo.close()
                        web = open("/home/fabrizio/Escritorio/hidss/HIPS2020/hips2020/hips2020/3.txt", "a")
                        web.write(str((logs_alarma)))
                        web.close()

#comprueba_dns ()
